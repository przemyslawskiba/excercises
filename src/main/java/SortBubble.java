/**
 * Created by pskiba on 11.07.2017.
 */
public class SortBubble {

    public static void main(String[] args) {

        {

            boolean test = true;

            int[] table = {1,4,7,5,2,9};


            while (test) {

                test = false;
                for (int i = 0; i < table.length - 1; i++) {

                    if (table[i] < table[i + 1]) {
                        int b = table[i];
                        table[i] = table[i + 1];
                        table[i + 1] = b;
                        test = true;
                    }
                }
            }

            for(int i=5; i>-1; i--){
                System.out.print(table[i]+" ");
            }
        }

    }
}
