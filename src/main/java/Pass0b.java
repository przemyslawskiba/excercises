/**
 * Created by pskiba on 16.06.2017.
 */
public class Pass0b {

    public static void main(String[] args) {

        Test ob1 = new Test(100,100);
        Test ob2 = new Test(100,100);
        Test ob3 = new Test(100, 1);

        System.out.println(ob1.equalsTo(ob2));
        System.out.println(ob2.equalsTo(ob3));

        System.out.println(ob3.a);
        System.out.println(ob3.b);

        ob3.meth(ob2);
        System.out.println(ob2.equalsTo(ob3));

        System.out.println(ob3.a);
        System.out.println(ob3.b);
    }
}

