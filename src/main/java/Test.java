/**
 * Created by pskiba on 16.06.2017.
 */
public class Test {

    int a,b;

    Test(int i, int j) {
        a= i;
        b = j;
    }

    boolean equalsTo(Test o) {
        if (o.a == a && o.b == b) return true;
        else return false;
    }

    Test(int i) {
        a = i;
    }

    Test incrByTen() {
        Test temp = new Test(a+10);
        return temp;
    }

    void meth (Test o) {
        a = 100;
        b = 100;
    }
}
