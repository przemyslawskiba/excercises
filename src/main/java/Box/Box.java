package Box;

/**
 * Created by pskiba on 16.06.2017.
 */
public class Box {

    private double width;
    private double height;
    private double depth;

    /* void print() {
         System.out.println("obj=" +width*height*depth);
     }
 */
    double volume() {
        return width * height * depth;
    }

    Box(double width, double height, double depth) {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }

    Box(double len) {
        this.depth = this.height = this.width = len;
    }

    Box() {
        this.depth = -1;
        this.width = -1;
        this.height = -1;
    }

    Box (Box ob) {
        this.depth = ob.depth;
        this.width = ob.width;
        this.height = ob.height;
    }


    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getDepth() {
        return depth;
    }

    public void setDepth(double depth) {
        this.depth = depth;
    }

    void show() {
        System.out.println(width);
        System.out.println(height);
        System.out.println("test");
    }
}

