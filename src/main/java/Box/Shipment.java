package Box;

import Box.BoxWeight;

/**
 * Created by pskiba on 16.06.2017.
 */
public class Shipment extends BoxWeight {

    double cost;

    public Shipment(double weight, double height, double depth, double width, double cost) {
        super(weight, height, depth, width);
        this.cost = cost;
    }
}
