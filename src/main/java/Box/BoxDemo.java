package Box;

/**
 * Created by pskiba on 16.06.2017.
 */
public class BoxDemo {

    public static void main(String[] args) {
        //Box.Box mybox = new Box.Box();
        double vol;

        /*
        mybox.depth = 10;
        mybox.height = 10;
        mybox.width = 25;

        vol = mybox.depth*mybox.height*mybox.width;

        System.out.println("obj=" +vol);

        mybox.setDim(10,20,30);
        */

        // Create new Box.Box and print value
        Box mybox = new Box(10,20,30);
        System.out.println("obj= " + mybox.volume());

        // Overload for Box.Box
        Box mybox1 = new Box();
        System.out.println("przeciazenie 1= " +mybox1.volume());
        Box mybox2 = new Box(2);
        System.out.println("przeciazenie 2= " +mybox2.volume());
        Box mybox3 = new Box(30,40,50);
        System.out.println("przeciazenie 3= " +mybox3.volume());


        // Clone class
        Box clonebox = new Box(mybox);
        System.out.println(clonebox.volume());

        // Class extends for weight
        BoxWeight mybox4 = new BoxWeight(5,3,2,3);
        System.out.println("waga= " + mybox4.getWeight());
        System.out.println("wysokosc= " + mybox4.getHeight());

        Shipment shipment = new Shipment(3,4,2,7,2);
        System.out.println(shipment.volume() + " " + shipment.cost + " " + shipment.getWeight());

        mybox4.show();
    }
}




