package Box;

/**
 * Created by pskiba on 16.06.2017.
 */
public class BoxWeight extends Box {

    private double weight;

    BoxWeight(double weight, double height, double depth, double width) {
        super(width, height, depth);
        this.setWeight(weight);

    }

    BoxWeight(BoxWeight ob) {
        super(ob);
    }

    BoxWeight() {
        super();
        weight = -1;
    }

    BoxWeight(double len ,double m) {
        super(len);
        weight = m;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    void show() {
        super.show();
        System.out.println("test");
    }
}
