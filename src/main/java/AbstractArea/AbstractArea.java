package AbstractArea;

/**
 * Created by pskiba on 17.06.2017.
 */

public class AbstractArea  {

    public static void main(String[] args) {


        Rectangle rectangle = new Rectangle(10,20);
        Triangle triangle = new Triangle(20,30);

        Figure figure;

        figure = rectangle;
        System.out.println("test rectangle=" + figure.area());

        figure = triangle;
        System.out.println("test traingle" + figure.area());
    }
}
