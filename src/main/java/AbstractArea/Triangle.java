package AbstractArea;

/**
 * Created by pskiba on 17.06.2017.
 */

class Triangle extends Figure {

    public Triangle(double dim1, double dim2) {
        super(dim1, dim2);
    }

    @Override
    double area() {

        System.out.println("pole trojkata");
        return dim1*dim2/2;
    }
}
