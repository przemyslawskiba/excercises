package AbstractArea;

/**
 * Created by pskiba on 17.06.2017.
 */

public class Rectangle extends Figure {

    public Rectangle(double dim1, double dim2) {
        super(dim1, dim2);
    }


    @Override
    double area() {
        System.out.println("przesloniecie Figure pole prostokata");
        return dim1*dim2;
    }
}


