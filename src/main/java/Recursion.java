/**
 * Created by pskiba on 16.06.2017.
 */
public class Recursion {

    public static void main(String[] args) {

        Factorial factorial = new Factorial();

        System.out.println(factorial.fact(4));
        System.out.println(factorial.fact(6));
    }
}
